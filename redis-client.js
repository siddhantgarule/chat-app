const redis = require("redis");
const client = redis.createClient();

const addMessage = (key, val, exp) => {
  client.set(key, val);
  if(exp) {
    client.expire(key, exp);
  }
};

const getRoomMessages = (socket, room) => {
  key = room + "-*";
  client.keys(key, (err, messageKeys) => {
    if(messageKeys) {
      client.mget(messageKeys, (err, messages) => {
        console.log('messages', messages);
        socket.emit('all-room-messages', JSON.stringify(messages));
        return messages;
      });
    }
  });
}

const addRoomMember = function(roomName){
    var key = roomName;
    client.get(key, function(err, reply) {
        if(!reply){
            client.set(key, "1");
        }
        else{
            var newClientNumber = Number.parseInt(reply) + 1;
            client.set(key, newClientNumber.toString())
        }
    });
}

const removeRoomMember = function(roomName){
    var key = roomName;
    client.get(key, function(err, reply) {
        if(!reply){
            client.set(key, "0");
        }
        else{
            var newClientNumber = Number.parseInt(reply) - 1;
            client.set(key, newClientNumber.toString());
        }
    });
}

const getStats = function() {
    var key = "room-*";
    client.keys(key, function(err, rooms){
        if(rooms){
            client.mget(rooms, function(err, roomsStats){
                var response = {};
                for(var i = 0; i < rooms.length; i++){
                    response[rooms[i]] = roomsStats[i];
                }
                console.log(response);
                return response;
            })
        }
    });
}

module.exports = {
  addRoomMember,
  removeRoomMember,
  getStats,
  addKey: addMessage,
  getRoomMessages
};
