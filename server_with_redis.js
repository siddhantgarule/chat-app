var app = require('express')();
var http = require('http').Server(app);
var port = process.env.PORT || 5002;
var redis = require("redis");
const uuidv4 = require('uuid/v4');
var client = redis.createClient();

app.get('/', function(req, res){
  res.send('chat server running!!!')
});

const addMessage = (key, val, exp) => {
  client.set(key, val);
  if(exp) {
    client.expire(key, exp);
  }
};

const getRoomMessages = (socket, room, channel) => {
  key = room + "-*";
  client.keys(key, (err, messageKeys) => {
    if(messageKeys) {
      client.mget(messageKeys, (err, messages) => {
        console.log('messages', messages);
        var data = {
            messages: messages,
            room: room
        }
        socket.emit(channel, JSON.stringify(data));
        return messages;
      });
    }
  });
}

var clientConnected = function(){
    var key = "chat-client";
    client.get(key, function(err, reply) {
        if(!reply){
            client.set(key, "1");
        }
        else{
            var newClientNumber = Number.parseInt(reply) + 1;
            client.set(key, newClientNumber.toString())
        }
    });
}

var clientDisconnected = function(){
    var key = "chat-client";
    client.get(key, function(err, reply) {
        if(!reply){
            client.set(key, "0");
        }
        else{
            var newClientNumber = Number.parseInt(reply) - 1;
            client.set(key, newClientNumber.toString())
        }
    });
}

var addRoomMember = function(roomName){
    var key = "room-"+roomName;
    client.get(key, function(err, reply) {
        if(!reply){
            client.set(key, "1");
        }
        else{
            var newClientNumber = Number.parseInt(reply) + 1;
            client.set(key, newClientNumber.toString())
        }
    });
}

var removeRoomMember = function(roomName){
    var key = "room-"+roomName;
    client.get(key, function(err, reply) {
        if(reply){
            var newClientNumber = Number.parseInt(reply) - 1;
            client.set(key, newClientNumber.toString())
        }
    });
}

var getStats = function(io, channel){
    var key = "room-*";
    client.keys(key, function(err, rooms){
        if(rooms){
            client.mget(rooms, function(err, roomsStats){
                var response = [];
                for(var i = 0; i < rooms.length; i++){
                    response.push({
                        "room_name": rooms[i].replace("room-",""),
                        "count": roomsStats[i]
                    })
                }
                console.log(response);
                io.emit(channel, response);
            })
        }
    })
}

const io = require('socket.io')(http, {
  path: '/',
  serveClient: false,
  // below are engine.IO options
  pingInterval: 10000,
  pingTimeout: 5000,
  cookie: false
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
  });
});
// handle incoming connections from clients
io.sockets.on('connect', function(socket) {
    // once a client has connected, we expect to get a ping from them saying what room they want to join
    socket.on('room', function(room) {
        socket.join(room);
        addRoomMember(room);
        getRoomMessages(socket, room, 'all-room-messages');
        setTimeout(function(){ getStats(io, 'get-stats');}, 2000);
    });

    socket.on('get-stats', function(msg){
      getStats(io, 'get-stats');
    });

    socket.on('get-room-history', function(room){
      getRoomMessages(socket, room, 'room-history');
    });

    socket.on('admin-message', function(msg){
        const room = msg.room;
        const key = room + '-' + uuidv4();

        addMessage(key, msg.message, msg.expires ? msg.expires : 100);
        io.to(msg.room).emit('chat-message', msg.message);
        setTimeout(function(){ getRoomMessages(socket, room, 'room-history');}, 2000);
    })

    socket.on('disconnecting', function(msg){
        var a = socket;
        if(socket.rooms){
            var arr = Object.keys(socket.rooms);
            arr.forEach(function(room){
                removeRoomMember(room);
            })
            setTimeout(function(){ getStats(io, 'get-stats');}, 2000);
        }
    });

    socket.on('disconnect', function(msg){
        var a = socket;
        clientDisconnected();
    });

    clientConnected();
});


http.listen(port, function() {
  console.log('listening on *:' + port);
});
