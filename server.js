const app = require('http').createServer(handler)
const io = require('socket.io')(app);
const fs = require('fs');
const uuidv4 = require('uuid/v4');

const redisClient = require('./redis-client');

app.listen(8001);

const roomsMap = {
  '/cart': 'cart',
  '/payment': 'payment'
};

function handler (req, res) {
  fs.readFile(__dirname + '/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}

io.on('connection', function (socket) {
  socket.on('room', (route) => {
    const room = roomsMap[route];
    socket.join(roomsMap[route]);
    redisClient.addRoomMember(room);
    const msg = redisClient.getRoomMessages(socket, room);
  });

  socket.on('announcement', function (data) {
    const room = data.room;
    const key = room + '-' + uuidv4();

    io.sockets.in(room).emit('announcement', data.message);
    redisClient.addKey(key, data.message, 10);

    console.log(data);
  });
});
